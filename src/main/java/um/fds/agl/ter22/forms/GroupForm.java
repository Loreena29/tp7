package um.fds.agl.ter22.forms;

import um.fds.agl.ter22.entities.Group;


public class GroupForm {
    private String name;
    private long id;


    public GroupForm(Long id, String Name) {
        this.name = Name;
        this.id = id;
    }


    public GroupForm() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name){this.name=name;}
}
