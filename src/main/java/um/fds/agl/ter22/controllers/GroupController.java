package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Group;
import um.fds.agl.ter22.forms.GroupForm;
import um.fds.agl.ter22.services.GroupService;

@Controller
public class GroupController implements ErrorController {


    @Autowired
    private GroupService groupService;


    @GetMapping("/listGroups")
    public Iterable<Group> getGroups(Model model) {
        Iterable<Group> groups=groupService.getGroups();
        model.addAttribute("group", groups);
        return groups;
    }
    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| !hasRole('ROLE_TEACHER')")
    @GetMapping(value = { "/addGroup" })
    public String showAddGroupPage(Model model) {

        GroupForm groupForm = new GroupForm();
        model.addAttribute("groupForm", groupForm);

        return "addGroup";
    }

    @PostMapping(value = { "/addGroup"})
    public String addGroup(Model model, @ModelAttribute("GroupForm") GroupForm groupForm) {
        Group g;
        if(groupService.findById(groupForm.getId()).isPresent()){
            // group already existing : update
            g = groupService.findById(groupForm.getId()).get();
            g.setName(groupForm.getName());
        } else {
            // group not existing : create
            g=new Group(groupForm.getName());
        }
        groupService.saveGroup(g);
        return "redirect:/listGroups";

    }

    @GetMapping(value = {"/showGroupUpdateForm/{id}"})
    public String showGroupUpdateForm(Model model, @PathVariable(value = "id") Long id){

        GroupForm groupForm = new GroupForm(id, groupService.findById(id).get().getName());
        model.addAttribute("groupForm", groupForm);
        return "updateGroup";
    }

    @GetMapping(value = {"/deleteGroup/{id}"})
    public String deleteGroup(Model model, @PathVariable(value = "id") Long id){
        groupService.deleteGroup(id);
        return "redirect:/listGroups";
    }


}
