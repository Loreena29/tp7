package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.repositories.TeacherRepository;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class SubjectController implements ErrorController {


    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/listSubjects")
    public Iterable<Subject> getSubjects(Model model) {
        Iterable<Subject> subjects=subjectService.getSubjects();
        model.addAttribute("subjects", subjects);
        return subjects;
    }
    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_TEACHER')")
    @GetMapping(value = { "/addSubject" })
    public String showAddSubjectPage(Model model) {

        SubjectForm subjectForm = new SubjectForm();
        model.addAttribute("subjectForm", subjectForm);

        return "addSubject";
    }

    @PostMapping(value = { "/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        Subject s;
        if(subjectService.findById(subjectForm.getId()).isPresent()){
            // subject already existing : update
            s = subjectService.findById(subjectForm.getId()).get();
            s.setName(subjectForm.getName());
            s.setDesc(subjectForm.getDesc());
            s.setTeacher(subjectForm.getTeacher());
            s.setTeacher2(subjectForm.getTeacher2());
        } else {
            // subject not existing : create
            s=new Subject(subjectForm.getName(), subjectForm.getDesc(), subjectForm.getTeacher(),subjectForm.getTeacher2());
        }
        subjectService.saveSubject(s);
        return "redirect:/listSubjects";

    }

    @GetMapping(value = {"/showSubjectUpdateForm/{id}"})
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id){

        SubjectForm subjectForm = new SubjectForm(id, subjectService.findById(id).get().getName(), subjectService.findById(id).get().getDesc(), subjectService.findById(id).get().getTeacher(),subjectService.findById(id).get().getTeacher2());
        model.addAttribute("subjectForm", subjectForm);
        return "updateSubject";
    }

    @GetMapping(value = {"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id){
        subjectService.deleteSubject(id);
        return "redirect:/listSubjects";
    }


}
