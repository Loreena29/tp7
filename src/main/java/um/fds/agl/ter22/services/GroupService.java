package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.Group;
import um.fds.agl.ter22.repositories.GroupRepository;

import java.util.Optional;

@Service
public class GroupService {

    @Autowired
    private GroupRepository groupRepository;

    public Optional<Group> getGroup(final Long id) {
        return groupRepository.findById(id);
    }

    public Iterable<Group> getGroups() {
        return groupRepository.findAll();
    }

    public void deleteGroup(final Long id) {
        groupRepository.deleteById(id);
    }

    public Group saveGroup(Group Group) {
        Group savedGroup = groupRepository.save(Group);
        return savedGroup;
    }

    public Optional<Group> findById(Long id) {
        return groupRepository.findById(id);
    }
}
