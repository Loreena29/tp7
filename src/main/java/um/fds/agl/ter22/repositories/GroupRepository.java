package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.Group;

public interface GroupRepository<T extends Group>
        extends CrudRepository<T, Long> {

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_STUDENT')")
    Group save(@Param("group") Group group);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_STUDENT')")
    void delete(@Param("group")Group group);

    @PreAuthorize("hasRole('ROLE_MANAGER')"+"|| hasRole('ROLE_STUDENT')")
    void deleteById(@Param("id") Long id);
}
