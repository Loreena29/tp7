package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Group {

    private @Id @GeneratedValue Long id;

    private String Name;

    public Group(String Name){
        this.Name = Name;
    }

    public Group(Long id, String Name){
        this.id=id;
        this.Name = Name;
    }


    public Group(){    }

    public Long getId() { return this.id; }

    public void setId(Long id){ this.id=id; }

    public String getName(){return this.Name;}

    public void setName(String name){this.Name=name;}

}
